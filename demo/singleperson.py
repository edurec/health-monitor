import os
import sys

sys.path.append(os.path.dirname(__file__) + "/../")

from scipy.misc import imread

from util.config import load_config
from nnet import predict
from util import visualize
from dataset.pose_dataset import data_to_input

import cv2


cfg = load_config("./pose-tf/demo/pose_cfg.yaml")

# Load and setup CNN part detector
sess, inputs, outputs = predict.setup_pose_prediction(cfg)


def process_full_body(frame):
	image_batch = data_to_input(frame)

	# Compute prediction with the CNN
	outputs_np = sess.run(outputs, feed_dict={inputs: image_batch})
	scmap, locref, pairwise_diff = predict.extract_cnn_output(outputs_np, cfg)

	# Extract maximum scoring location from the heatmap, assume 1 person
	pose = predict.argmax_pose_predict(scmap, locref, cfg.stride)
	#arrows = predict.argmax_arrows_predict(scmap, locref, pairwise_diff, cfg.stride)

	visualize.awesome_show_arrows(cfg, frame, pose, scmap)
	return visualize.visualize_joints(frame, pose)


# Read image from file
#file_name = "demo/image.png"
#image = imread(file_name, mode='RGB')

# cap = cv2.VideoCapture(0)

# while True:
# 	ret, frame = cap.read()
# 	image_batch = data_to_input(frame)

# 	# Compute prediction with the CNN
# 	outputs_np = sess.run(outputs, feed_dict={inputs: image_batch})
# 	scmap, locref, pairwise_diff = predict.extract_cnn_output(outputs_np, cfg)

# 	# Extract maximum scoring location from the heatmap, assume 1 person
# 	pose = predict.argmax_pose_predict(scmap, locref, cfg.stride)
# 	arrows = predict.argmax_arrows_predict(scmap, locref, pairwise_diff, cfg.stride)
# 	print (arrows)
# 	# Visualise
# 	#visualize.show_heatmaps(cfg, frame, scmap, pose)
# 	visualize.awesome_show_arrows(cfg, frame, pose, scmap)
# 	cv2.imshow("lala", visualize.visualize_joints(frame, pose))
# 	#sleep(5)
# 	#visualize.show_arrows(cfg, frame, pose, arrows)
# 	#visualize.waitforbuttonpress()
# 	if cv2.waitKey(1) ==13: 
# 		break
		
# cap.release()
# cv2.destroyAllWindows()
