from PyQt5.QtWidgets import QApplication, QMainWindow, QLabel, QGridLayout, QWidget, QCheckBox, QSystemTrayIcon, \
    QSpacerItem, QSizePolicy, QMenu, QAction, QStyle, qApp, QVBoxLayout, QTabWidget
from PyQt5.QtCore import QSize, QThread, pyqtSignal, pyqtSlot, Qt, QTimer
from PyQt5.QtGui import QImage, QPixmap, QIcon
import cv2
import blink
import numpy as np
from enum import Enum
import sys
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
import pandas as pd
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()

import singleperson

TRAY_MODE = False
WINDOW = 500
Q_WINDOW = 1000
SMALL_WINDOW = 10
Q = 0.1


class TabType(Enum):
    FATIGUE_AND_EYES_HEALTH = 0
    POSTURE = 1


TAB_NAMES = {TabType.FATIGUE_AND_EYES_HEALTH: "Fatigue and eyes health", TabType.POSTURE: "Posture"}


def is_camera_available(source):
    cap = cv2.VideoCapture(source)
    return cap is not None and cap.isOpened()


class PlotCanvas(FigureCanvas):
    def __init__(self, parent=None, manager=None, width=5, height=4, dpi=100):
        fig = Figure(figsize=(width, height), dpi=dpi)
        self.axes = fig.add_subplot(111)

        FigureCanvas.__init__(self, fig)
        self.setParent(parent)
        self.manager = manager

        FigureCanvas.setSizePolicy(self, QSizePolicy.Expanding, QSizePolicy.Expanding)
        FigureCanvas.updateGeometry(self)

        self.compute_initial_figure()

        timer = QTimer(self)
        timer.timeout.connect(self.update_figure)
        timer.start(1000)

    def compute_initial_figure(self):
        self.axes.plot(self.manager.data.log, 'r')

    def update_figure(self):

        try:
            n = len(self.manager.data)
            if n < 1:
                return

            begin = max(0, n - WINDOW)
            end = n - 1

            log = self.manager.data["log"]
            log_smoothed = log.rolling(SMALL_WINDOW, min_periods=1).mean()[begin:]
            time = self.manager.data["timestamp"][begin:]

            log_q = log.rolling(Q_WINDOW, min_periods=1).quantile(Q)[begin:]

            duration = (self.manager.data["timestamp"][end] - self.manager.data["timestamp"][begin]).total_seconds()
            fps = len(log_smoothed) / duration

            idx = log_smoothed < log_q

            times = blink.calculate_times(idx) + 0.001

            avg_closed = sum(idx) / times / fps
            avg_open = sum(~idx) / times / fps
            times_per_minute = times / duration * 60

            # print("duration=", duration)
            # print("fps=", fps)
            # print("times =", times)
            # print("avg_open =", avg_open)
            # print("avg_closed=", avg_closed)
            # print("times_per_minute=", times_per_minute)

            self.manager.table_widget.avg_times.setText("Average blinks number/minute: {:10.1f}".format(times_per_minute))

            self.axes.cla()

            self.axes.plot(time, log_smoothed, "b-")
            self.axes.plot(time, log_q, "r-")
            self.axes.set_title('Eye aspect ratio')
            self.draw()

        except Exception as e:
            print(e)


class Thread(QThread):
    changePixmap = pyqtSignal(QImage)

    def __init__(self, parent=None, camera_index=0):
        super().__init__(parent)
        self.camera_index = camera_index

    def run(self):
        cap = cv2.VideoCapture(self.camera_index)

        while True:
            ret, frame = cap.read()

            #facial features only for the first camera
            self.parent().process_frame(frame, self.camera_index)

            if ret:
                # https://stackoverflow.com/a/55468544/6622587
                rgbImage = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                h, w, ch = rgbImage.shape
                bytesPerLine = ch * w
                convertToQtFormat = QImage(rgbImage.data, w, h, bytesPerLine, QImage.Format_RGB888)
                p = convertToQtFormat.scaled(640, 480, Qt.KeepAspectRatio)
                self.changePixmap.emit(p)


class MainWindow(QMainWindow):
    """
         Сheckbox and system tray icons.
         Will initialize in the constructor.
    """
    @pyqtSlot(QImage)
    def setImage(self, image):
        self.label.setPixmap(QPixmap.fromImage(image))

    @pyqtSlot(QImage)
    def setImage1(self, image):
        self.label1.setPixmap(QPixmap.fromImage(image))


    def initUI(self):
        #self.setWindowTitle(self.title)
        #self.setGeometry(self.left, self.top, self.width, self.height)
        self.resize(640, 480)
        # create a label
        self.label = QLabel(self)
        self.label.resize(640, 480)

        camera_index = 0
        th = Thread(self, camera_index)
        th.changePixmap.connect(self.setImage)
        th.start()

        if is_camera_available(1) :
            self.label1 = QLabel(self)
            #self.label1.move(0, 0)
            self.label1.resize(640, 480)
            th1 = Thread(self, 1)
            th1.changePixmap.connect(self.setImage1)
            th1.start()

    # Override the class constructor
    def __init__(self):
        # Be sure to call the super class method
        QMainWindow.__init__(self)
        self.data = pd.DataFrame(columns=["log", "timestamp"])
        self.initUI()

        self.setMinimumSize(QSize(600, 900))             # Set sizes
        self.setWindowTitle("Yoco")  # Set a title
        central_widget = QWidget(self)                  # Create a central widget
        self.setCentralWidget(central_widget)           # Set the central widget

        grid_layout = QGridLayout(self)         # Create a QGridLayout
        central_widget.setLayout(grid_layout)   # Set the layout into the central widget
        # grid_layout.addWidget(QLabel("Application, which can minimize to Tray", self), 0, 0)

        # Add a checkbox, which will depend on the behavior of the program when the window is closed
        #self.check_box = QCheckBox('Debug mode', self)
        #self.check_box.setEnabled(True)
        #grid_layout.addWidget(self.check_box, 1, 0)
        #grid_layout.addItem(QSpacerItem(0, 0, QSizePolicy.Expanding, QSizePolicy.Expanding), 2, 0)

        grid_layout.addWidget(self.label, 2, 0)

        #two cameras case
        if hasattr(self, 'label1'):
            self.setMinimumSize(QSize(1200, 900))             # Set sizes
            grid_layout.addWidget(self.label1, 2, 1)

        self.table_widget = MyTableWidget(self)
        if not hasattr(self, 'label1'):
            grid_layout.addWidget(self.table_widget, 3, 0)
        else:
            grid_layout.addWidget(self.table_widget, 3, 0, 1, 2)

        # Init QSystemTrayIcon
        self.tray_icon = QSystemTrayIcon(self)
        self.tray_icon.setIcon(self.style().standardIcon(QStyle.SP_ComputerIcon))

        '''
            Define and add steps to work with the system tray icon
            show - show window
            hide - hide window
            exit - exit from application
        '''
        show_action = QAction("Show", self)
        quit_action = QAction("Exit", self)
        hide_action = QAction("Hide", self)
        show_action.triggered.connect(self.show)
        hide_action.triggered.connect(self.hide)
        quit_action.triggered.connect(qApp.quit)
        tray_menu = QMenu()
        tray_menu.addAction(show_action)
        tray_menu.addAction(hide_action)
        tray_menu.addAction(quit_action)
        self.tray_icon.setContextMenu(tray_menu)
        self.tray_icon.show()

    def process_frame(self, frame, camera_index):
        current_index = 0
        if hasattr(self, 'table_widget'):
            current_index = self.table_widget.tabs.currentIndex()
        
        if camera_index == 0:
            self.data = blink.process_frame(frame, self.data, current_index)

        if current_index == 1 and camera_index == 1:
            frame = singleperson.process_full_body(frame)


    # Override closeEvent, to intercept the window closing event
    # The window will be closed only if there is no check mark in the check box
    def closeEvent(self, event):
        if TRAY_MODE:
            event.ignore()
            self.hide()
            self.tray_icon.showMessage(
                "Tray Program",
                "Application was minimized to Tray",
                QSystemTrayIcon.Information,
                2000
            )


class MyTableWidget(QWidget):
    def __init__(self, parent):
        super(QWidget, self).__init__(parent)

        # Initialize tab screen
        self.tabs = QTabWidget(self)

        box_layout = QVBoxLayout(self)

        for type in TabType:
            # Add tabs
            atr_name = "tab{}".format(type.value)
            setattr(self, atr_name , QWidget())
            tab = getattr(self, atr_name)

            self.tabs.addTab(tab, TAB_NAMES[type])

            tab.layout = QVBoxLayout(tab)

        self.tabs.resize(300, 200)

        self.canvas = PlotCanvas(self, self.parent())
        self.tab0.layout.addWidget(self.canvas)

        self.avg_times = QLabel()
        self.avg_times.setText("Average blinks number/minute: 0.0")
        self.tab0.layout.addWidget(self.avg_times)


        self.tab0.setLayout(self.tab0.layout)

        # Add tabs to widget
        box_layout.addWidget(self.tabs)
        self.setLayout(box_layout)


if __name__ == "__main__":
    app = QApplication(sys.argv)
    app_icon = QIcon()
    for s in [16, 32, 64, 128, 256]:
        app_icon.addFile('icons/{}.png'.format(s), QSize(s, s))

    app.setWindowIcon(app_icon)
    mw = MainWindow()
    mw.show()
    sys.exit(app.exec())