from scipy.spatial import distance
from imutils import face_utils
import imutils
import dlib
import cv2
import numpy as np
import datetime
import collections
import math

detect = dlib.get_frontal_face_detector()
predict = dlib.shape_predictor("shape_predictor_68_face_landmarks.dat")
(lStart, lEnd) = face_utils.FACIAL_LANDMARKS_IDXS["left_eye"]
(rStart, rEnd) = face_utils.FACIAL_LANDMARKS_IDXS["right_eye"]

DEVICE = 0


def eye_aspect_ratio(eye):
    A = distance.euclidean(eye[1], eye[5])
    B = distance.euclidean(eye[2], eye[4])
    C = distance.euclidean(eye[0], eye[3])
    return (A + B) / (2.0 * C)


def calculate_times(idx):
    in_word = False
    counter = 0
    for i, value in idx.items():
        if not in_word and value:
            counter += 1
        in_word = value

    return counter

def to_int(ft):
    return (int(ft[0]), int(ft[1]))

def construct_head_axes(im, image_points, current_index):
    size = im.shape

    NOSE_PNT_INDEX = 33

    points2D = np.array([image_points[NOSE_PNT_INDEX], 
                image_points[8],
                image_points[45],
                image_points[36],
                image_points[54],
                image_points[48]
                ], dtype="double")

# 3D model points.
    model_points = np.array([
                                (0.0, 0.0, 0.0),             # Nose tip
                                (0.0, -330.0, -65.0),        # Chin
                                (-225.0, 170.0, -135.0),     # Left eye left corner
                                (225.0, 170.0, -135.0),      # Right eye right corne
                                (-150.0, -150.0, -125.0),    # Left Mouth corner
                                (150.0, -150.0, -125.0)      # Right mouth corner
                            ])

    focal_length = size[1]
    center = (size[1]/2, size[0]/2)
    camera_matrix = np.array(
                             [[focal_length, 0, center[0]],
                             [0, focal_length, center[1]],
                             [0, 0, 1]], dtype = "double"
                             )
     
    dist_coeffs = np.zeros((4,1)) # Assuming no lens distortion
    (success, rotation_vector, translation_vector) = cv2.solvePnP(model_points, points2D, camera_matrix, dist_coeffs, flags=cv2.SOLVEPNP_ITERATIVE)
     
    for p in points2D:
        cv2.circle(im, (int(p[0]), int(p[1])), 3, (0,0,255), -1)

    if current_index != 1:
        return

    axis = np.float32([[500,0,0], 
                       [0,500,0], 
                       [0,0,500]])
                          
    imgpts, jac = cv2.projectPoints(axis, rotation_vector, translation_vector, camera_matrix, dist_coeffs)
    modelpts, jac2 = cv2.projectPoints(model_points, rotation_vector, translation_vector, camera_matrix, dist_coeffs)
    rvec_matrix = cv2.Rodrigues(rotation_vector)[0]

    proj_matrix = np.hstack((rvec_matrix, translation_vector))
    eulerAngles = cv2.decomposeProjectionMatrix(proj_matrix)[6]

    pitch, yaw, roll = [math.radians(_) for _ in eulerAngles]

    pitch = math.degrees(math.asin(math.sin(pitch)))
    roll = -math.degrees(math.asin(math.sin(roll)))
    yaw = math.degrees(math.asin(math.sin(yaw)))

    p1 = ( int(image_points[NOSE_PNT_INDEX][0]), int(image_points[NOSE_PNT_INDEX][1]))

    color = (0,255,0)
    if 10 < abs(roll) < 20:
        color = (0, 255, 255)
    elif abs(roll) >= 20: #danger zone
        color = (0,0,255)

    cv2.line(im, p1, to_int(tuple(imgpts[1].ravel())), color, 3) #GREEN
    cv2.line(im, p1, to_int(tuple(imgpts[0].ravel())), (255,0,255), 1) #BLUE
    cv2.line(im, p1, to_int(tuple(imgpts[2].ravel())), (255,0,0), 1) #RED



LAST_N_SHAPES = 3
last_shapes = collections.deque()

def process_frame(frame, data, current_index):
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    faces = detect(gray, 0)

    n = len(faces)

    if n < 1:
        last_shapes.clear()
        return data

    face = faces[0]

    curr_shape = predict(gray, face)

    shape = face_utils.shape_to_np(curr_shape)  # converting to NumPy Array

    if len(last_shapes) == LAST_N_SHAPES:
        last_shapes.popleft()

    last_shapes.append(shape)

    shape = np.mean(last_shapes, axis=0).astype(int)

    left_eye = shape[lStart:lEnd]
    right_eye = shape[rStart:rEnd]
    left_ear = eye_aspect_ratio(left_eye)
    right_ear = eye_aspect_ratio(right_eye)
    ear = (left_ear + right_ear) / 2.0
    left_eye_hull = cv2.convexHull(left_eye)
    right_eye_hull = cv2.convexHull(right_eye)
    cv2.drawContours(frame, [left_eye_hull], -1, (0, 255, 0), 1)
    cv2.drawContours(frame, [right_eye_hull], -1, (0, 255, 0), 1)


    construct_head_axes(frame, shape, current_index)

    face_hull = cv2.convexHull(shape)
    area = cv2.contourArea(face_hull)
    
    # detect constant from calibration
    if area > frame.shape[0] * frame.shape[1] * 0.15:
        cv2.drawContours(frame, [face_hull], -1, (0, 0, 255), 5)

    return data.append({"log": ear,  "timestamp": datetime.datetime.now()}, ignore_index=True)


def process_video(cap):
    log = np.array([], dtype=float)
    log_median = np.array([], dtype=float)
    log_smoothed = np.array([], dtype=float)

    time_start = datetime.datetime.now()
    while True:
        ret, frame = cap.read()

        frame = imutils.resize(frame, width=450)

        log, log_smoothed, log_median = process_frame(frame, log, log_smoothed, log_median)

        cv2.imshow("Frame", frame)
        key = cv2.waitKey(1) & 0xFF
        if key == ord("q"):
            break
    cv2.destroyAllWindows()
    cap.release()

    duration = (datetime.datetime.now() - time_start).total_seconds()
    fps = len(log) / duration

    idx = log_smoothed < log_median

    times = calculate_times(idx)

    avg_closed = sum(idx) / times / fps
    avg_open = sum(~idx) / times / fps
    times_per_minute = times / duration * 60

    print("duration=", duration)
    print("fps=", fps)
    print("times =", times)
    print("avg_open =", avg_open)
    print("avg_closed=", avg_closed)
    print("times_per_minute=", times_per_minute)

    return {"log": log, "log_smoothed": log_smoothed, "log_median": log_median, "duration": duration, "fps": fps,
            "times": times, "avg_open": avg_open, "avg_closed": avg_closed, "times_per_minute": times_per_minute}


if __name__ == "__main__":
    # cap = cv2.VideoCapture(DEVICE)
    cap = cv2.VideoCapture("masha.mp4")
    process_video(cap)

